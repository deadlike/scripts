import requests
from terminaltables import DoubleTable


class GenericRepr:
    def __repr__(self):
        items = ("{}={!r}".format(k, self.__dict__[k]) for k in self.__dict__.keys())
        return "{}({})".format(type(self).__name__, ", ".join(items))


class DataMixin(GenericRepr):

    def __init__(self, data):
        self.__dict__.update(data)


class Location(DataMixin):
    pass


class SensorSpecs(DataMixin):
    pass


class SensorType(DataMixin):
    pass


class SensorValue(DataMixin):
    pass


class Sensor(GenericRepr):
    def __init__(self, json_data):
        self.timestamp = json_data.get('timestamp')
        self.sampling_rate = json_data.get('sampling_rate')
        self.id = json_data.get('id')
        self.sensor_pin = json_data.get('sensor', {}).get('pin')
        self.sensor_id = json_data.get('sensor', {}).get('id')
        self.sensor_type = SensorType(json_data.get('sensor', {}).get('sensor_type'))
        self.location = Location(json_data.get('location', {}))
        self.sensordatavalues = [SensorValue(data) for data in json_data.get('sensordatavalues')]
        self._get_t1_t2_values()

    def _get_t1_t2_values(self):
        for sensor_value in self.sensordatavalues:
            if sensor_value.value_type == 'P1':
                self.p1 = sensor_value.value
            if sensor_value.value_type == 'P2':
                self.p2 = sensor_value.value


def get_api_data(sensor_id):
    r = requests.get(f'https://data.sensor.community/airrohr/v1/sensor/{sensor_id}/')
    return [Sensor(data) for data in r.json()]


def main():
    try:
        sensor = get_api_data(10001)[1]
    except IndexError:
        raise IndexError('Api returned only one element.')

    table_data = [['Timestamp', 'P10', 'P2.5'], [sensor.timestamp, sensor.p1, sensor.p2]]
    table = DoubleTable(table_data)
    print(table.table)


if __name__ == '__main__':
    main()



