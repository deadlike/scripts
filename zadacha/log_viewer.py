#!/usr/bin/env python3

import os
import sys
import re
from collections import Counter

try:
    log_location = sys.argv[1]
except IndexError:
    log_location = os.path.join(os.getenv('HOME'), 'nginx-log')
finally:
    print('Using log file: {}'.format(log_location))

if not os.path.isfile(log_location):
    print('No such file')

ip_re = re.compile(r'''
                   (\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})
                   (.*?)
                   (\s{1}S=\d{1,3}\s{1})
                   (.*?)
                   (T=\d+\.\d{1,3})
                   ''', re.VERBOSE)

result = {}
with open(log_location, 'r') as log_file:
    for line_number, log_line in enumerate(log_file):
        match = ip_re.search(log_line)
        if match:
            ip, _, status, _, time = match.groups()
            # print(ip, status, time)
            status = status.strip().replace('S=', '')
            time = float(time.strip().replace('T=', ''))
            if ip not in result.keys():
                result[ip] = {'status_codes': [status],
                              'times': [time],
                              }
            else:
                result[ip]['status_codes'].append(status)
                result[ip]['times'].append(time)
        else:
            print('Skipping line {}, log:{}'.format(line_number, log_line))
    # print(result)

for ip, stats in result.items():
    elements = Counter(stats['status_codes'])
    print('----\n'.rjust(4), 'IP: ' + ip)
    for status_code, number in elements.items():
        print('\tStatus code: {} occured: {} time/s.'.format(status_code,
                                                             number,
                                                             ))
    avg_time = round(sum(stats['times'])/len(stats['times']), 5)
    print('\tAverage time:', avg_time)
