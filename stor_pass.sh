#!/bin/bash
 
# Exit on error.
error() {
  printf "%b\n" "ERROR: $@" >&2 
  exit 1
}
 
usage () {

cat <<EOF
Usage: ${0#./} OPTION
 
The following options are supported:
 
  -d  --decrypt
    Decrypt ./secrets.tar.gz.enc to ./secrets
 
  -e  --encrypt
    Encrypt files in ./secrets to ./secrets.tar.gz.enc
 
All options prompt for a password. Use the password provided to you.
EOF
}
 
# Get a normalized absolute path to the password directory.
DIR="$( cd "${0%/*}" && pwd)"
UNENCRYPTED_DIRNAME="secrets"
UNENCRYPTED_DIR="${DIR}/${UNENCRYPTED_DIRNAME}"
UNENCRYPTED_DIR_BAK="${UNENCRYPTED_DIR}-bak"
UNENCRYPTED_ARCHIVE="${DIR}/secrets.tar.gz"
ENCRYPTED_ARCHIVE="${UNENCRYPTED_ARCHIVE}.enc"
 
if [[ "$#" -ne 1 ]]; then
  usage
  exit 1
fi
 
case "${1}" in
  # Decrypt from archive.
  -e|--decrypt)
 
    openssl des3 -salt -d \
      -in "${ENCRYPTED_ARCHIVE}" \
      -out "${UNENCRYPTED_ARCHIVE}"
 
    if [ -d "${UNENCRYPTED_DIR}" ]; then
      rm -Rf "${UNENCRYPTED_DIR_BAK}"
      mv "${UNENCRYPTED_DIR}" "${UNENCRYPTED_DIR_BAK}"
    fi
 
    tar -C "${DIR}" -zvxf "${UNENCRYPTED_ARCHIVE}"
    rm -f "${UNENCRYPTED_ARCHIVE}"
    ;;
 
  # Encrypt to archive.
  -e|--encrypt)
    rm -f "${UNENCRYPTED_ARCHIVE}"
    tar -C "${DIR}" -zcvf "${UNENCRYPTED_ARCHIVE}" "${UNENCRYPTED_DIRNAME}"
 
    openssl des3 -salt \
      -in "${UNENCRYPTED_ARCHIVE}" \
      -out "${ENCRYPTED_ARCHIVE}"
 
    rm -f "${UNENCRYPTED_ARCHIVE}"
    ;;
 
  # Utility options.
  -h|--help)
    usage
    exit 0
    ;;
 
  *)
    error "Unrecognized option ${1}"
    ;;
esac
