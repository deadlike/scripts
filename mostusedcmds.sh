#!/bin/bash

#Write down the history. 
history -w

shopt -s checkwinsize
#History file to be used. 
histf="$HOME/.bash_history"
#Gets the screen size of the terminal.
scrnsz="$(tput cols)"
#Default value for the number of commands that will be shown.
cmdnm=15
#If there is command line argument, which is a number, it will be set as a valude for the number of commands to be shown.
if [[ "$1" =~ [[:digit:]] ]]; then
	cmdnm="$1"
fi

#Gets the list of most used commands starting from the top to bottom and saves them into an array.
mapfile -t cmds < <( 	awk '{print $1}' "$histf"\
			| sort\
		       	| uniq -c\
		       	| sort -rn\
		      	| grep -vE  "#"\
		       	| awk '{print $2}' )
#Saves the number of times of each ran command in the below array using the same logic as the above array.
mapfile -t cmdcount < <( awk '{print $1}' "$histf"\
			| sort\
		   	| uniq -c\
		       	| sort -rn\
		      	| grep -vE  "#"\
		       	| awk '{print $1}' )
#Gets the longest command, so the display width can be calculated correctly. 
cmdlength=$( printf "%s\\n" "${cmds[@]}" | head -n "$cmdnm" | awk '{print length($0) " " $0; }' | sort -rn | awk 'FNR == 1 {print $1}')

sep(){
	#Just a seperator function. So it can be prettier.
	for ((j=0;j<scrnsz;j++));do
		printf "="
	done
	echo
}

#Checks if the command number variable is bigger than the array indexes, if it is, the array total legth will become the number of commands shown.This is to prevent empty lines. 
if  (( ${#cmds[@]} < "${cmdnm}")); then
	cmdnm="${#cmds[@]}"
fi
 
#Display width is the screenszie minus the lenth of the longest command. 
wdisp="$(( scrnsz - cmdlength))" 
sep
#Loops through the number of commands stopping at the value specified in the cmdnm variable. 
for (( i=0; i<cmdnm; i++)); do  

	#Save the number of commands ran in a one more variable so we can latter check its length and modify it if we need to.
	n[i]="${cmdcount[i]}"	
	#print the command adjusted by its legth
	#printf > echo
	printf "%*s " "${cmdlength}" "${cmds[$i]}"  
	#Check if the command leght is longer than the display of the screen. 
	if (( n[i] > wdisp )); then 
		#If it is adjust it so it does not go to a new line. 
		n[i]="$(( wdisp - 5 ))"
	fi	
	#Loop thorugh the legth and print # so it looks fancier.
	for ((j=0;j<n[i];j++));do

		printf "#"
	done
	#print the command at the end.
	printf " %u\\n" "${cmdcount[$i]}"
done
sep
