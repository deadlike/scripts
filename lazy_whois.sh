#!/bin/bash

#################################################
# Made by Vic U.
# This is a lazy whois/dig script check. It checks whether the domain has a ccTLD or an ICANN whois record and executes the proper lazy whois script.
# The check is nessaccary as the script's infromation based on the WHOIS and domains with different registries display the whois differently. Ex: .ca vs .com (check how the NS are displayed).
################################################
#TODO: More detailed documentation.
#Sources the library.
#TODO: FIX the HOME variable so the script can be installed anywhere besides the home dir.
#lib_find="$( find "$HOME" -type f -iname "lib_lazy_whois.sh" 2>/dev/null)"

. lib_lazy_whois.sh
#if [[ -n "$lib_find" ]]; then

#	. "$lib_find"

#else
#	printf "Unable to locate/read the lib_lazy_whois.sh library!" >&2
#fi

#traps the SIGINT and SIGTERM and instead exits the script with the below message. Also deletes the temp directory if one's created during the script execution. 
trap 'rm -rf "$HOME/tmp/" && exit' SIGTERM
trap 'printf "\n%bAborted by signal 2...%b\n" "$RED" "$NORMAL" && exit 130' SIGINT SIGQUIT 
#Kills any child processes of the group on script exit.
trap 'SPEED && kill 0' EXIT

#Clears the screen and scrollback
#clear && printf '\033[3J'

#Lopps through the arguments and checks for any options
#TODO: fix that nested loop it is no needed
for i; do 
	while getopts ":A:hH:d:" opt; do
		case $opt in 
			h|H)
			ERR USAGE 
			;;
			A)
			NS="$OPTARG"
			;;
			d )
			dom+=("$OPTARG")
			;;
			\?)
			printf -- "Invalid option: -%s!\n" "$OPTARG" >&2
			ERR
			;;
			:)
			printf -- "The option: -%s requires an argument!\n" "$OPTARG" >&2
			ERR
			;;
		esac
	done
	(( ++OPTIND ))
done

#Checks if the NS option is set and the option argument has a dot in it, then uses 2 functions from the lib_lazy_whois.sh library on them.
#TODO:Fix the REGEX
if [[ -n "$NS" ]] && [[  "$NS" =~ \. ]]; then

	for z in "${dom[@]}"; do
		if [[  "$z" =~ \. ]];then
			MATCH_IP "$z" "$NS"
       			DIG_MX "$z" "$NS" 
		else
			ERR NS
		fi
	done
	exit 0;
fi
#Enrsures the first element of the dom array is set
if [[ -z "$dom" ]]; then
	
	dom=("$1")
fi

#If no optional NS  are used, loops through the array and runs all of the queries defined in the lib_lazy_whois.sh library. 
#The last two displayed queries are ran as jobs as they take longer, the output is saved in a file. 
#The scripts waits for the jobs to complete and shows the file's output.
for j in "${dom[@]}"; do

	if [[ ! "$j" =~ ^[[:alnum:]]+(\-{0,1}[[:alnum:]]+)+(\.{1}[[:alnum:]]+)+$ ]]; then
		
		ERR input 
	else
	
		#Calls the functions defined in the lib_lazy_whois.sh library.
		if [[ -w  "$HOME" ]]; then

			mkdir "$HOME/tmp/" 2> /dev/null
			_ran1="$HOME/tmp/${j}_1_$RANDOM"	
			_ran2="$HOME/tmp/${j}_2_$RANDOM"
			export -f "WHOIS"
		        export	"WHITE" "NORMAL"
			if ! timeout 5 bash -c "WHOIS $j"; then
				printf "1)%b[%bFAILED%b] %b%s%b\n" "$NORMAL" "$RED" "$NORMAL" "$RED" "The WHOis look up took more than 5 sec to respond. Skipping..." "$NORMAL"
			fi
			MATCH_IP "$j" > "$_ran1" &	
			DIG_MX "$j" > "$_ran2" &
			FAST_DIG "$j" 
			wait
			cat "$_ran1" "$_ran2" 2>/dev/null
		
		else
			printf "The scripts directory is not writable!" >&2
			exit 1;
		fi
	fi
done 
