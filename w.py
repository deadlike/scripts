#!/usr/bin/env python3

import sys, re
import dns.resolver
import whois 
import ipwhois


def DomainWhois(domain):
    w = whois.whois(domain)
    for k,v in w.items():
        if v == None or k == 'name_servers':
            continue
        print(k.title().replace('_',' ').rjust(15),end=':\n')
        if type(v) is list:
            for i in v:
                print('\t\t-->',i)
        else:
            print('\t\t-->',v)
def queryDomain(domain):
    res = dns.resolver.Resolver(configure=False)
    res.nameservers = ['1.1.1.1']
    res.timeout = 5
    res.lifetime = 1

    records = ['A','MX','TXT','NS']
    lst = {}
    ip_addr = {}

    for record in records:
        try:
            for data in res.query(domain, record):
                    lst.setdefault(record,[]).append(data)
        except dns.resolver.NoAnswer:
                lst.setdefault(record,[]).append(None)

    for item in lst['A']:
        try:
            domain_addr = dns.reversename.from_address(str(item))
            reversed_dns = str(res.query(domain_addr,"PTR")[0])
        except dns.resolver.NXDOMAIN:
            reversed_dns = None
        ip_addr.setdefault('PTR',reversed_dns)
        ip_info = ipwhois.IPWhois(item).lookup_whois(inc_nir=True)
        ip_addr.setdefault(item,set()).add(ip_info['nets'][0]['description'].title())
        ip_addr.setdefault(item,set()).add(ip_info['asn_description'].title())

    print('\nDNS records for:'.rjust(60, '='), domain)
    for key, value_list in lst.items():
        print('\nType: '.rjust(20,'-') , key)
        for value in sorted(value_list):
            print('  --> ', str(value).rstrip('.'))
            if value in ip_addr:
                for v in ip_addr[value]:
                    print('\t--> ',v)
                print('  PTR --> ',ip_addr['PTR'])

def matchDomain(string):
    reDomain = re.compile(r'''
                 (\w)+
                 (\.{1})
                 (\w|\d)+
                ''', re.VERBOSE )    
    domain = reDomain.search(string)
    if domain == None:
        print('Error! Not a domain.')
    else:
        return domain.group()[::-1]

try:
    domain = sys.argv[1]
except IndexError:
    domain = input('Please enter a domain: ')

domain = matchDomain(domain.strip('.').lower()[::-1])
DomainWhois(domain)
queryDomain(domain)
