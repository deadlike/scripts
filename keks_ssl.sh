#!/bin/bash

# This script matches RSA PKIs and CERTs 
# Made by Vic U.
#Color codes
BLACK="\033[30m"
RED="\033[31m"
GREEN="\033[32m"
YELLOW="\033[33m"
BLUE="\033[34m"
PINK="\033[35m"
CYAN="\033[36m"
WHITE="\033[37m"
NORMAL="\033[0;39m"
WHITE='\e[1;37m'
LIGHT_GREEN='\e[1;32m'
LIGHT_BLUE='\e[1;34m'
LIGHT_RED='\e[1;31m'
LIGHT_PURPLE='\e[1;35m'



VAR(){
	#This function takes input from STDIN and removes any blank lines
	#By input is meant .pem PKI and CERTs
	
	local TXT
	TXT=$(</dev/stdin); printf "%b" "$TXT" | sed '/^$/d'

};

COUNT(){

	#This is a pretty counter that I made. 
	#It has no purposes. 
	#I just spend some time to make it and it's pretty so I added it in this script!

	local count ran
	count=0
	tput civis
	ran="$(( RANDOM % 100 ))"
	printf "\nProgress...\n(0%%)["
	while [[ $count -le 100 ]] ; do 

	
		sleep 0.018
		printf "#"
		tput sc
		printf "](%d%%)" "$count"
		tput rc	1
	
		if [[ $count -eq $ran ]]; then
			
			sleep 1
	
		elif [[ $count -eq 100 ]]; then
	
			printf "](100%%)"
			tput el
			break
		fi
	
		(( count++ ))
	done
	tput cnorm
}


PROMPT() { 
	
	#A standard prompt
	
	case $1 in

	PKI) 
	printf "%bEnter a%b PKI%b, once you are ready%b make sure the cursor is on a new line%b, then click %bCTRL +D: %b\n" "$WHITE" "$RED" "$WHITE" "$RED" "$WHITE" "$RED" "$NORMAL"
	;;
	CERT)  
	printf "%bEnter a%b CERT%b, once you are ready%b make sure the cursor is on a new line%b, then click %bCTRL +D: %b\n" "$WHITE" "$RED" "$WHITE" "$RED" "$WHITE" "$RED" "$NORMAL"
	;;
	BOTH | *)
	printf "%bEnter a%b PKI/CERT%b, once you are ready%b make sure the cursor is on a new line%b, then click %bCTRL +D: %b\n" "$WHITE" "$RED" "$WHITE" "$RED" "$WHITE" "$RED" "$NORMAL"
	;;

esac


}

ERROR(){
	
	#Just a standard error

	tput bold
	printf "\n%bERROR!¯\(°_o)/¯ %b\n" "$RED" "$NORMAL" >&2	
	case $1 in 

	PKI)
	printf	"%bYou must include both the header(-----BEGIN PRIVATE KEY-----) and footer(-----END PRIVATE KEY-----) for the PKI!\n\n" "$WHITE" "$NORMAL" 
	;;
	CERT)
	printf	"%bYou must include both the header(-----BEGIN CERTIFICATE-----) and footer(-----END CERTIFICATE-----) for CERT!\n\n%b" "$WHITE" "$NORMAL"
	;;
	ARG)
	printf "%bToo many arguments!%b\\n\\n" "$RED" "$NORMAL"
	;;
	*)
	tput bold
	printf "%bThis is neither a CERT nor a PKI!%b\n\n" "$RED" "$NORMAL"
	;;
	esac >&2 
	tput sgr0
	sleep 1.5
}

	
if [[ "$#" -eq 0 ]]; then 
	
	PROMPT 
	TEXT="$(VAR)"
elif [[ "$#" -le 2 ]]; then

	for i; do
		if [[ ! -s "$i" ]]; then
			shift
		fi
	done
	TEXT="$( { cat "$1" | sed '/^$/d'; } 2> /dev/null)"
	INFO="$( { cat "$2" | sed '/^$/d'; } 2> /dev/null)"
	
else
	ERROR ARG
	exit 1;
fi


	if printf "%s\n\n" "$TEXT" | openssl x509 -noout 2> /dev/null; then

		if [[ -z "$INFO" ]]; then

			PROMPT PKI
			INFO="$(VAR)"
			while ! printf "%s\n\n" "$INFO" | openssl pkey -noout 2> /dev/null; do
	
				ERROR PKI	
				PROMPT PKI
				INFO="$(VAR)"
			done
		fi
		CERT="$TEXT"
		PKI="$INFO"	
	elif printf "%s\n\n" "$TEXT" | openssl pkey -noout 2> /dev/null; then 	

		
		if [[ -z "$INFO" ]]; then
			PROMPT CERT
			INFO="$(VAR)"
			while ! printf "%s\n\n" "$INFO" | openssl x509 -noout 2> /dev/null; do

				ERROR CERT
				PROMPT CERT
				INFO="$(VAR)"
			done
		fi
		CERT="$INFO"
		PKI="$TEXT"	
	else
		ERROR 
		exit 1;
	fi

	PKI_MOD=$(printf "%b\n\n" "$PKI" | openssl rsa -check -noout -modulus | sed '/RSA key ok/d' | openssl md5) 
	CERT_MOD=$(printf "%b\n\n" "$CERT" | openssl x509 -noout -modulus | openssl md5)

	if printf "%b\n" "$PKI_MOD" | grep -w "$CERT_MOD" > /dev/null 2>&1;then
  	
       		COUNT	
		printf 	"\n%bThe entered PKI's mod is:  %s\nThe entered CERT's mod is: %s\nThey match!\nThe PKI and CERT will be displayed in:\n" "$LIGHT_GREEN" "$PKI_MOD" "$CERT_MOD" 
   
  		printf "\n%b\n\n%b\n\n" "$PKI" "$CERT" 
	else
	
		COUNT
		printf 	"\n%bThe entered PKI's mod is:  %s\nThe entered CERT's mod is: %s\nThey don't match!\nLeaving....\n" "$LIGHT_RED" "$PKI_MOD" "$CERT_MOD" 

	fi

