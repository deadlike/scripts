#!/bin/bash


#List of websites that would be queried to get your IP address. They are saved in the below array.
list=("ifconfig.me" "ipecho.net/plain" "ifconfig.co" "icanhazip.com" "ipinfo.io/ip" )

#Command line parameters can be supplied in the form of URLs and they will be appended to the above array.
if [[ -n "$1" ]]; then
	for z; do 
		list+=( "$z" )
	done	
fi

#Save all of the IP adresses in a list_IP array
for i in "${list[@]}"; do
	list_IP+=( "$(curl -sw '\n' "$i")" )
done

#Matches all elemetns of the above array with eachother and prints the ones that are not the same.
#the below variable represents the next array element
t=1
for j in "${list_IP[@]}"; do
	for q in "${list_IP[@]:$t}"; do 
		if [[ "$j" != "$q" ]]; then
			printf "Public IP: %s\n" "$q"	
			break
		fi
	done
	(( ++t ))
done

_host="$(hostname -A)"
_localIP="$(hostname -I)"
printf "Public IP: %s\nLocal IP: %s\nHostname: %s\n" "${list_IP[0]}" "$_localIP" "$_host"
