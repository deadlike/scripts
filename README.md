# Bash Scripts

Those are just some scripts that I created while learning bash scripting.

# How to install:

````
cd; git clone https://gitlab.com/deadlike/scripts.git 
`````
Optional:

- Set up automatic updates(on login) for the local repository(master branch):

````
echo '[[ -w "$HOME/scripts" ]] && git -C "$HOME/scripts" pull origin master || printf "Could not write to the ./scripts directory!"' >> "$HOME/.bashrc"; 
````

- Append your $PATH variable as shown below so the scripts are run as commands instead of going to the git repo and running ./scriptame.sh. This has to be done only ones.

`````
echo 'export PATH="$PATH:$HOME/scripts"' >> "$HOME/.bashrc" ; . "$HOME/.bashrc"  
`````

# Brief description of each script:

* blacklist_check.sh - checks IP against the most popular RBLs. 

* getcsr - generate a CSR

* install(TBD)

* ranpass.sh - generate a random password. 

* show_publicip.sh - shows public IP.

* mostusedcmds.sh - shows the most used commands.

* SSL_info.sh - removes the base 64 encoding of SSL certificates/CSRs.

* TLS.sh - a one liner that checks the SSL certificate for a website(if it has one).

* convert_ssl.sh - converts SSL certificates into different formats. #This one is not finished.

* keks_ssl.sh - checks if a private key matches a certificate.

* lazy_whois.sh - checks the whois information for a domain and some network information(A,MX,TXT,autodiscover).
* lib_lazy_whois.sh - library for the above one.

* o365.sh - queries the o365 records to see of a domain uses the service(-h for help).
* ranPass.sh - generates a random password
* blacklist_check.sh - checkes if an IP address is blacklisted using the RBLs in RBL.long or RBL.short(default)
____
Made by Vic.


