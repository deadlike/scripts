#!/bin/bash

erR(){
	tabs 4
	printf "\n\tUsage: 
	%s <password length> [option] (number of passwords)\n
	if <password length> is not specified, a 16 character password will be generated. Cannot be less than 6 charaters.
	if [option] is not specified, a password composed of lowercase, uppercase and digit charaters will be generated.
	if (number of passwords) is not specified, 5 passwords will be generated.\n
	Additional Options:
	Specify the number of passwords and their length: 
	%s <N>\t\t\t\t\t\t\tspecify password length
	%s <N> [-N] (number of passwords)\tspecify the number of passwords\n
	Specify additional characters:
	%s <N> [-p] \t\t\t\t\t\tGenerates 5 passwords and punctuation symbols may be used.
	%s <N> [-P] (number of passwords)\tGenerates the number of passwords specified as an argument and punctuation symbols may be used.
	%s <N> [-s] \t\t\t\t\t\tGenerates 5 passwords and spaces may be used.
	%s <N> [-S] (number of passwords)\tGenerates the number of passwords specified as an argument and spaces may be used.
	%s <N> [-a] \t\t\t\t\t\tGenerates 5 passwords and both punctuation and spaces may be used.
	%s <N> [-A] (number of passwords)\tGenerates the number of passwords specified as an argument and both punctuation and spaces may be used.\n\n" "${0##*/}" "${0##*/}" "${0##*/}" "${0##*/}" "${0##*/}" "${0##*/}" "${0##*/}" "${0##*/}" "${0##*/}"
	exit 1;
}

WHI(){
	local count 
	count=1
	if [[  $NUMBER =~ [[:alpha:]]|[[:punct:]]|[[space]] ]]; then
		printf "Illegal option! Use -h for help\n" 
		exit 1;
	fi
	
	printf "Generating %s random %s character passwords:\n\n" "${NUMBER:="5"}" "${num:="16"}" 
			
	while [[ $count -le $NUMBER ]] ;do 
	
		tabs 4	
		printf "\t%s.\t" "$count"
	
		case $1 in 
			-N)
			tr -dc "[:alnum:]"  < /dev/urandom | head -c "$num" | xargs -0 
			;;
			-S) 
			tr -dc "[:alnum:]\ "  < /dev/urandom | head -c "$num" | xargs -0 
			;;
			-P)
			tr -dc "[:graph:]"  < /dev/urandom | head -c "$num" | xargs -0 
			;;
			-A)
			tr -dc "[:print:]"  < /dev/urandom | head -c "$num" | xargs -0 
			;;
		esac
		(( ++count  ))
	
	done
	exit 0;
}

if [[ $# -eq 0 ]]; then 
	
	printf "No options found setting default ones!\nUse -h for more options.\n\n"
	WHI -N 

else 	
		
	if  [[ "$1" =~ [[:digit:]] ]] ; then
		num="$1"	
		if (( num < 6 ));then
			printf "This password is insecure!\n"
			exit 1
		fi	
		shift
	fi
	for i; do	
		while getopts ":P:pS:sA:ahN:" opt; do

			case $opt in
				p)
				WHI -P	
				;;	
			       	P)
				NUMBER="$OPTARG"
				WHI -P	
				;;
				s )
				WHI -S
				;;	
				S)
				NUMBER="$OPTARG"
				WHI -S
				;;
				a)
				WHI -A	
				;;
		       		A)
				NUMBER="$OPTARG"
				WHI -A	
				;;
				h)
				erR
				;;
				N)
				NUMBER="$OPTARG"
				;;
				\?)
				printf "Illegal option, use -h for help.\n"
				exit 1;
				;;
				:) 
				printf "This option \"-$OPTARG\" requires an argument! Leaving...\n"
				exit 1;
				;;
			esac
		done
        	(( ++OPTIND ))
	done	
 	WHI -N	
fi
