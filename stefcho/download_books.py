#!/usr/bin/env python3.8
import contextlib
import shutil
import sys
from concurrent.futures import ProcessPoolExecutor, ThreadPoolExecutor
from dataclasses import dataclass, field
from pathlib import Path
from typing import ClassVar, Iterable, List, Set, Tuple

import requests
from bs4 import BeautifulSoup
from loguru import logger

DOWNLOAD_URL = "https://porn.jules-aubert.info/humble_bundle/"
DEFAULT_SAVE_PATH = Path.home() / "Downloads" / "books"
DEFAULT_SAVE_PATH.mkdir(exist_ok=True, parents=True)

# Those hrefs are for navigations, no books in them
DIR_LISTING_HREFS = {
    "?C=N;O=A",
    "?C=M;O=A",
    "?C=S;O=A",
    "?C=D;O=A",
    "?C=N;O=D",
    "/",
    "/humble_bundle/",
}


@dataclass
class BookDir:
    """Represents a book directory"""

    name: str
    book_downloads_in_parallel: int = 40

    books: List[str] = field(default_factory=list)

    @property
    def url(self) -> str:
        return DOWNLOAD_URL + self.name

    @property
    def save_path(self) -> Path:
        return DEFAULT_SAVE_PATH / self.name

    def find_books(self) -> None:
        request = requests.get(self.url)
        self.books = sorted(find_all_hrefs_on_page(request.text), key=lambda x: Path(x).suffix)
        logger.info(f"Found {len(self.books)} books in directory {self.name}")

    def _download_and_save_book(self, book) -> Tuple[str, Path]:
        book_path = self.save_path / book
        if book_path.exists():
            raise FileExistsError(f"Book {book} already exists at {book_path}")
        logger.info(f"Downloading book {self.url}{book}")
        with requests.get(self.url + book, stream=True) as request:
            request.raise_for_status()
            with (self.save_path / book).open("wb") as f:
                shutil.copyfileobj(request.raw, f)
        return book, book_path

    def download_books(self) -> None:
        self.save_path.mkdir(exist_ok=True)
        try:
            with ThreadPoolExecutor(max_workers=self.book_downloads_in_parallel) as executor:
                for book, book_path in executor.map(self._download_and_save_book, self.books):
                    logger.info(f"Book {book} saved in {book_path}")
        except FileExistsError as e:
            logger.error(e)


def find_all_hrefs_on_page(html_page: str) -> List[BookDir]:
    soup = BeautifulSoup(html_page, "lxml")
    return [href for link in soup.find_all("a") if (href := link.get("href")) not in DIR_LISTING_HREFS]


def main() -> None:
    request = requests.get(DOWNLOAD_URL)
    request.raise_for_status()
    dir_hrefs = find_all_hrefs_on_page(request.text)
    logger.info(f"Found {len(dir_hrefs)} directories with books at {DOWNLOAD_URL}")

    book_dirs = [BookDir(dir_href) for dir_href in dir_hrefs]
    with ThreadPoolExecutor(max_workers=10) as executor:
        for book_dir in book_dirs:
            executor.submit(book_dir.find_books)

    with ProcessPoolExecutor(max_workers=10) as executor:
        for book_dir in book_dirs:
            executor.submit(book_dir.download_books)


if __name__ == "__main__":
    main()
