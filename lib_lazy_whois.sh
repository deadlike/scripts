#!/bin/bash

####TODO: Optimize the library further.
####TODO: Detailed documentation.
####TODO: Make the library smaller and easier to understand.

#Colour codes, some of them are not used. 
BLACK='\e[30m'
RED='\e[38;5;160m'
GREEN='\e[38;5;28m'
YELLOW='\e[33m'
BLUE='\e[38;5;44m'
PINK='\e[35m'
CYAN='\033[38;5;57m'
NORMAL='\e[0;39m'
WHITE='\e[1;37m'
LIGHT_GREEN='\e[1;32m'
LIGHT_BLUE='\033[38;5;32m'
LIGHT_RED='\e[1;31m'
ORANGE='\033[38;5;202m'
GOLD='\e[38;5;222m'

DOM_CHECK(){
	
	#Gets the name servers for a domain from a "random" tld NS
	local domain tld _ran tld_NS NS
	domain="$1"
	tld="${domain##*.}"
	_ran="$(printf "%s\\n" {a..m} | shuf | head -n 1)"
	tld_NS=$(dig NS "$domain" @"$_ran".ROOT-SERVERS.NET. | grep "^$tld" | awk 'END {print $5}')
	NS=$(dig NS "$domain" @"$tld_NS" | grep "^$domain" | awk 'FNR == 1 {print $5}') 
	printf "%s\\n" "$NS" 
};

WHOIS(){
	#This function checks if a domain has CIRA/ICANN based whois record, then it does a "clean"(removes useless text, which whois -H does not remove) whois for a domain.
	local domain LIST
	domain="$1"
	#The below list can be expanded with more domain's that have similiar whois record to the existing ones below
	LIST="\.(ca|co\.uk|eu|as|bg|com\.mx|de|mx)";
	
	if printf "%s" "${domain,,}" | grep -Ei "$LIST" >/dev/null;then
		
		local WHOIS
		WHOIS=$(whois -H 2>/dev/null "$domain" | awk '/Domain/,/WHOIS/'\
      		 	                               | sed -e '/^$/d' -e '/%/d')
    
		printf "1)The FULL whois information for %s:\\n%b%s\\n%b" "$domain" "$WHITE" "$WHOIS" "$NORMAL"
		
	else
	
		#TODO: The ICANN whois record "clean up" can be done in a smart way, instead of just removing text from the whois record.
		local WHOIS min_WHOIS
		
		WHOIS=$(whois -H 2>/dev/null "$domain" | sed -E -e '/URL/d'\
        			                                -e '/>>>/d'\
                        			                -e '/NOTICE/,/^\s*$/d'\
                                	                      	-e '/For/d'\
                           	        	   		-e '/%/d'\
                                			        -e '/NeuStar/,/^\s*$/d'\
                                       		  		-e '/Registrar WHOIS Server/d'\
                                        			-e '/Registrar Abuse Contact/d'\
                                     	    			-e '/^$/d'\
                                        			-e '/Registry Registrant ID/d'\
                                    		      		-e '/Registry Admin ID/d'\
                                        			-e '/Registry Tech ID:/d'\
                                          			-e '/Registrar IANA ID/d'\
								-e '/The data in the BlueHost.Com/,/www.bluehost.com/d'\
								-e '/The Data in MarkMonitor.com/,/--/d'\
						        	-e '/.CO Internet/d'\
								-e '/If certain contact information/,/ocumentation and explanation./d');
							#| awk -F":"  '{ print "\033[1;37m" $1":""\033[38;5;208m"$2,$3,$4,$5  }' );

		min_WHOIS=$(printf "%s/n" "$WHOIS" | grep -e ^"Registrar:"\
        	 	                                  -e "Domain Status"\
	        		                          -e "Registry Expiry Date:"\
					   	   	  -e "Registrar Registration Expiration Date"\
							  -e "Name Server:"\
						   | awk -F":"  '{ print "\033[1;37m" $1":""\033[38;5;147m"$2,$3,$4,$5  }' );
		printf "1)The FULL whois information for %s:\\n%b%s\\n%b The valuable whois information for %s:\\n%b%s\\n%b" "$domain" "$WHITE" "$WHOIS" "$NORMAL" "$domain" "$WHITE" "$min_WHOIS" "$NORMAL"
	fi
	
};

MATCH_IP(){

	#The function checks if the network record for a domain, matches at least one in the name servers.
	#This is done in order to see if changes were made to the DNS records.

	local domain NS OPT_NS
	domain="$1"
	OPT_NS="$2"
	
	if [[ -z "$OPT_NS" ]]; then

		NS="$(DOM_CHECK "$domain")"	
	else
		NS="$OPT_NS"
	fi

	#Matches the network IP with the one from the authoritative query.
	local IP IP_NS
	IP="$(dig +short "$domain")"
	IP_NS="$(dig +short "${domain}" @"${NS:?Could not get NS!}" )"

	IP_DIG(){
		
		#Nested function. It was created in order to avoid repetition in the below if statement
		local IP_WHOIS WHO IP_LIST
		case $@ in 
			
			NS)
			IP_LIST="$IP_NS"	
			;;
			*)
			IP_LIST="$IP"
			;;
		esac
		if [[ -z "$IP_LIST" ]]; then
			return 1
		fi	
		for WHO in $IP_LIST; do 
			{	
                        IP_WHOIS="$(whois -H 2>/dev/null "$WHO"  | grep -E "(OrgName|CustName|organisation|role):"\
							         | uniq \
							         | tr -d '  ')"
			printf "%s -> %s\\n" "$WHO" "$IP_WHOIS"
		}&
		done
		wait
		return 0;
	};

		fail(){

    	    		printf "%b[%bFAILED%b] %b%s%b\n" "$NORMAL" "$RED" "$NORMAL" "$RED" "Does not resolve!" "$NORMAL"
		};

	printf "%b8)Checks if the A record for %s exists and matches the one in %b%s\\n" "$WHITE" "$domain" "$PINK" "$NS"	
	
	if echo "$IP"  | grep -w "$IP_NS" >/dev/null ; then
 		
		printf "%bResult of %bdig +short %s%b:%b\\n" "$WHITE" "$ORANGE" "$domain" "$WHITE" "$GREEN" 
	 	if ! IP_DIG; then 
		    fail
        fi
		
		printf "%bResult of %bdig +short %s %b@%s%b:%b\\n" "$WHITE" "$ORANGE" "$domain" "$PINK" "$NS" "$WHITE" "$GREEN"	
		if ! IP_DIG NS; then
		    fail
        fi
	else
		printf "%bResult of %bdig +short %s%b:%b\\n" "$WHITE" "$ORANGE" "$domain" "$WHITE" "$RED" 
		if ! IP_DIG; then 
		    fail
        fi
		printf "%bResult of %bdig +short %s %b@%s%b:%b\\n" "$WHITE" "$ORANGE" "$domain" "$PINK" "$NS" "$WHITE" "$RED"	
		if ! IP_DIG NS; then
		    fail
        fi
	fi

printf "%b" "$NORMAL"	
return 0;
};

DIG_MX(){

	#The function checkes if the MX network record matches the one in the domain's NS, also checks the IP to which it maps and its organization
	local domain NS OPT_NS
	domain="$1"
	OPT_NS="$2"
	if [[ -z "$OPT_NS" ]]; then

		NS="$(DOM_CHECK "$domain")"	
	else
		NS="$OPT_NS"
	fi

	local MX MX_NS
	MX="$(dig MX +short "$domain" | awk '{print $2}')"
	MX_NS="$(dig MX +short "$domain" @"${NS:?Could not get NS!}" | awk '{print $2}')"

	MX_IP(){
		 
		local IP IP_WHOIS MX_LIST i d 
		case $@ in 

			NS)
			MX_LIST="${MX_NS,,}"
			if [[ -z "$MX_LIST" ]];then
				return 1;
			fi
			for i in $MX_LIST; do
			{	
				IP="$(dig +short "$i" @"$NS")"
				if [[ -z $IP ]]; then
					IP="$(dig +short "$i")"
				fi	 
		
				for d in $IP; do
						
                        		IP_WHOIS="$(whois -H 2>/dev/null "$d"  | grep -E "(OrgName|CustName|organisation|role):"\
									       | uniq \
									       | tr -d '  ')"
					printf "%s-> %s -> %s\\n" "$i" "$d" "$IP_WHOIS"
			
				done
			} &
			done
			wait
			;;
			*)
			MX_LIST="${MX,,}"
			if [[ -z "$MX_LIST" ]];then
				return 1;
			fi
			for i in $MX_LIST; do
			{	
				IP="$(dig +short "$i")"
				
				for d in $IP; do
				
                        		IP_WHOIS="$(whois -H 2>/dev/null "$d"  | grep -E "(OrgName|CustName|organisation|role):"\
									       | uniq \
									       | tr -d '  ')"
					printf "%s-> %s -> %s\\n" "$i" "$d" "$IP_WHOIS"
				done
			} &
			done
			wait
			;;
		esac		
		
		};
	
	printf "%b8)Checks if the MX record for %s exists and matches the one in %b%s\\n" "$WHITE" "$domain" "$PINK" "$NS"	

		fail(){

    	    		printf "%b[%bFAILED%b] %b%s%b\n" "$NORMAL" "$RED" "$NORMAL" "$RED" "Does not resolve!" "$NORMAL"
		};

if echo "${MX,,}"  | grep -w "${MX_NS,,}" >/dev/null ; then
	
	printf "%bResult of %bdig MX +short %s%b:%b\\n" "$WHITE" "$ORANGE" "$domain" "$WHITE" "$GREEN"
 	if ! MX_IP; then
		fail	
	fi	
		printf "%bResult of %bdig MX +short %s %b@%s%b:%b\\n" "$WHITE" "$ORANGE" "$domain" "$PINK" "$NS" "$WHITE" "$GREEN"	
	if ! MX_IP NS;then
		fail
	fi
else
	
	printf "%bResult of %bdig MX +short %s%b:%b\\n" "$WHITE" "$ORANGE" "$domain" "$WHITE" "$RED"
	if ! MX_IP; then
		fail
	fi	
		printf "%bResult of %bdig MX +short %s %b@%s%b:%b\\n" "$WHITE" "$ORANGE" "$domain" "$PINK" "$NS" "$WHITE" "$RED"	
	if ! MX_IP NS;then
		fail
	fi
fi

printf "%b" "$NORMAL"	
return 0;	
};

FAST_DIG(){  
	MISSING(){
    local EC
    EC="$1"
	
	if [[ ! $EC -eq 0 ]]; then 
    	    printf "%b[%bFAILED%b] " "$NORMAL" "$RED" "$NORMAL"	
	    if (( EC == 124 )); then 
		    printf "%bTimed out! Skipping...\\n" "$RED" >&2
            return 1
        fi

    	printf "%bMissing!\\n" "$RED" >&2
	    return 1
	fi
	};

	#This function's idea is to dig specific DNS records and show them in a specific format.
	local domain _DIG
	domain="$1"
	_DIG="$(dig +short "$domain" | head -n 1)"
	
	#Digs for a network record for $1 and does a whois of it
	printf "2)The Network record/s for %s are:%b\\n" "$domain" "$LIGHT_BLUE"
	dig "$domain" +noall +answer | grep -Ei "^$domain";
	MISSING "$?" 
	
    printf "%b The IP belongs to:\\n%b" "$NORMAL" "$LIGHT_BLUE"
	timeout 2 whois -H "$_DIG" | grep -E "(OrgName|CustName|organisation|role):"  
	MISSING "$?" 
	#Gets the PTR record for the domain. Checks if it has timed out and if it has saves the 124 exit code in the EC variable.
	
    printf "%b3)PTR for %s is:\\n%b" "$NORMAL" "$domain" "$CYAN"
	timeout 2 dig -x "$_DIG" | grep -E "^[[:alnum:]]+.*PTR\b"
	MISSING "$?" 
    #Digs for www.$1 and does a whois of the IP #I like this dig format more than dig +short
	
    printf "%b4)The Network record/s for www.%s:%b\\n" "$NORMAL" "$domain" "$LIGHT_BLUE" #"$ORANGE"
	dig www."$domain" +noall +answer | grep -Ei ^www."$domain";
	MISSING "$?" 
	
    printf "%b The IP belongs to:\\n%b" "$NORMAL" "$LIGHT_BLUE"
	timeout 2 whois -H "$(dig +short "www.$domain" | head -n 1)" 2>/dev/null | grep -E "(OrgName|CustName|organisation|role):" 
	MISSING "$?" 
    ####Digs for MX.
	
    printf "%b5)The MX for %s is:\\n%b" "$NORMAL" "$domain" "$YELLOW"
	dig mx "$domain" +noall +answer | grep -Ei "^$domain";
	MISSING "$?" 
	####Digs for the autodiscover.
	
    printf "%b6)The autodiscover record for %s is:\\n%b" "$NORMAL" "$domain" "$GOLD"
	dig autodiscover."$domain" +noall +answer | grep -Ei "^autodiscover.$domain"
	MISSING "$?" 
	####Digs for TXT records.
	
    printf "%b7)The TXT record/s for %s are:\\n%b" "$NORMAL" "$domain" "$BLUE"
	dig TXT "$domain" +noall +answer | grep -Ei "^$domain"
	MISSING "$?" 
	printf "%b" "$NORMAL"
};

SPEED(){

	#Standard speed function.
	#TODO:Fix it to display the miliseconds as well
	printf "\\n%s ran %s on TTY%s for %s seconds ¯\_(ツ)_/¯\\n" "${USER^}" "${0##*/}" "$$" "$SECONDS"
	
};


ERR(){
	#Error and usage function	
	TEXT="$1"
	case "$TEXT" in 
		
		USAGE)
		printf "\t\tDescription:
			This script performs domain information gathering. \n\t\tSections:
	      		1) FULL whois look up.
			2) Network address look up and the network owner of the address. 
			3) Reverse DNS look up. 
			4) Same as 2) but for the www third level domain.
			5) MX record look up. 
			6) Network look up for autodiscover third level domain.
			7) TEXT record look up. 
			8) Network address look up matched against the network record in the domain's authoritative name servers.
			9) SAme as 8) but for the MX.\n\t\tUsage:	
			%s domain.tld [perfom look ups 1-9 for a specified domain]\n\t\t\tOptional arguments:
			%s -h [show this menu]
			%s -d domain.tld [specify a domain (can be used multiple times)]
			%s -A nsN.domain.tld [specify custom nameserver, where only 8) and 9) will be performed(requires the above option)]\n\n
			" "${0##*/}" "${0##*/}" "${0##*/}" "${0##*/}" 
		;;
		NS)
		printf	"Error! You have not specified a valid domain name/name server!\n"  
		;;
		input)	
		printf "This is not valid input!\n" 
		;;
		esac >&2

		printf "Use %s -h!\n
			⠰⡿⠿⠛⠛⠻⠿⣷
		⠀⠀⠀⠀⠀⠀⣀⣄⡀⠀⠀⠀⠀⢀⣀⣀⣤⣄⣀⡀
		⠀⠀⠀⠀⠀⢸⣿⣿⣷⠀⠀⠀⠀⠛⠛⣿⣿⣿⡛⠿⠷
		⠀⠀⠀⠀⠀⠘⠿⠿⠋⠀⠀⠀⠀⠀⠀⣿⣿⣿⠇
		⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⠉⠁
		
		⠀⠀⠀⠀⣿⣷⣄⠀⢶⣶⣷⣶⣶⣤⣀
		⠀⠀⠀⠀⣿⣿⣿⠀⠀⠀⠀⠀⠈⠙⠻⠗
		⠀⠀⠀⣰⣿⣿⣿⠀⠀⠀⠀⢀⣀⣠⣤⣴⣶⡄
		⠀⣠⣾⣿⣿⣿⣥⣶⣶⣿⣿⣿⣿⣿⠿⠿⠛⠃
		⢰⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡄
		⢸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡁
		⠈⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠁
⠀		⠀⠛⢿⣿⣿⣿⣿⣿⣿⡿⠟
	⠀	⠀⠀⠀⠉⠉⠉\\n" "${0##*/}" >&2		
		exit 1; 
};
