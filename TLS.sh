#!/bin/bash

#work in progress

TlsInfo(){

	local Domain
	Domain="$1"
	if ! command -v openssl > /dev/null 2>&1; then
		return 1
	fi

	local Dates
		Dates="$( openssl s_client -servername "$Domain" -connect "$Domain":443 <<< "Q" 2> /dev/null\
			| openssl x509 -noout -dates )"
		Issuer="$( openssl s_client -servername "$Domain" -connect "$Domain":443 <<< "Q" 2> /dev/null\
			| openssl x509 -noout -issuer )"
		Subject="$( openssl s_client -servername "$Domain" -connect "$Domain":443 <<< "Q" 2> /dev/null\
			| openssl x509 -noout -subject )"
		printf '%s\n%s\n%s\n' "$Dates" "$Issuer" "$Subject"

}
TlsInfo $@
