#!/bin/bash

# Don't judge the reason for why I made this script. I was bored.
# bUt YoU CoUld HaVe UsEd AnSibLe?????


#TODO: 	Everything should be logged and moved into the log dir.
#TODO:	Maybe add .ssh conf file for the git repo instead of using --global for the core.ssh command
#TODO:	More documentation.	
#TODO: 	Should find the scrips and create a symlinks.	
#TODO:	Should double check the path variable.
#TODO:	Should create systemd timers.
#TODO:	Should add proper timestamps.
#####The first date is used in logs, the second is used in the log name.
#####LDate="$(date +%T)"
#####NDate="$(date +%F)"
#TODO:	Logrotate check and config for the log dir.
#TODO: add a check to see if the conf file was sourced properly.
#TODO: FZF
#TODO: Options for variable assignment

#Colorful status function. Used to make show status and message of the commands being executed in a pretier way.
CStat(){
	case "$1" in
		S)
		printf 	"\\n%b[%bSUCCESS%b]  %b" "$NORMAL" "$GREEN" "$NORMAL" "$2"
		;;
		F)
		printf 	"\\n%b[%bFAILED%b]   %b" "$NORMAL" "$RED" "$NORMAL" "$2" >&2
		;;
		C)
		printf	"\\n%b[%bCAUTION%b]  %b" "$NORMAL" "$ORANGE" "$NORMAL" "$2" 
		;;
		I)
		printf 	"\\n%b[%bINPUT%b]  %b\\n" "$NORMAL" "$YELLOW" "$NORMAL" "$2" 
		;;	
	esac	
}

# Traps the exit signal, printing the below message, notifying the user the script has completed.
trap 'CStat S "Script comleted!\n"' EXIT

# Gets config values from install.conf.
. ./install.conf

# Gets the effective ID of the user running the script checking if he is root.
## To write in the default $InsPath(defined in install.conf) you have to be root.
_id="$(printf "%s\\n" "$EUID")"
if (( _id != 0)); then
	
	CStat C	"Warning! It is recommended to run this script as root!" 
fi

# Checks if git is installed. Script fails if it's not. 
if ! command -v git > /dev/null 2>&1; then
	
	CStat F "Fatal! Could not find git. Please ensure that it is properly installed!"
	exit 1
fi

# Checks if logrotate is installed.
#if ! command -v logrotate > /dev/null 2>&1; then
	
#	CStat F "Fatal! Could not find logrotate. Please ensure that it is properly installed!"
#	exit 1
#fi

# Loops through all of the variables defined in install.conf checking if any of them are empty.
## If a variable is set but empty the script exits.
for var in "${VarArr[@]}"; do 
       
	if [[ -z "$var" ]]; then
	
		CStat F "Fatal! The file install.conf contains an empty variable, please review the config."
		exit 1
	fi
done

# Checks if the install.conf file was edited. 
## If it wasn not, the default values are printed and the user running the script is asked if he would like to continue.
## More on that in the comments of install.conf file.
if [[ "$Edited" == "FALSE" ]]; then
	
	CStat S "The install.conf file was not modified, the default values will be used:"
	#Default values are hard coded below, just in case the user forgot to change the $Edited value in install.conf.
	GitRepo="git@gitlab.com:deadlike/scripts.git"
	GitRepoKey="FALSE"
	GitName="Jon Doe"
	GitEmail="jon@doe.ninja"
	InsPath="$HOME"
	SSHPath="$HOME/.ssh"
	SSHKey="NONE"
	SSHType="ecdsa"
	SSHBits="384"
	LogDir="$HOME/log"
	VimConf="FALSE"

elif [[ "$Edited" == "TRUE" ]]; then 

	CStat C "The install.conf file was modified, the new values will be used:"
else
	CStat F "Fatal! Unknown value \"$Edited\". Please review the install.conf file."  
	exit 1
fi

# Prints the values of the important values that will be used. 
printf "
\\tInstall path: \"%s\"
\\tLogging dir:  \"%s\"
\\tGit user:     \"%s\"
\\tGit email:    \"%s\"
\\tGit repo:     \"%s\"
\\tRequires SSH: \"%s\"
\\tRepo SSH key: \"%s\"\\n
\\tOptional SSH values for generating key pair:
\\tSSH dir path: \"%s\"
\\tSSH crypto:   \"%s\"
\\tSSH bits:     \"%s\"\\n
\\tMiscellaneous:
\\tVim theme:    \"%s\"
\\n" "$InsPath" "$LogDir" "$GitName" "$GitEmail" "$GitRepo" "$GitRepoKey" "$SSHKey" "$SSHPath" "$SSHType" "$SSHBits" "$VimConf"

CStat I "Would you like to continue?[Y/n]"
select answr in Yes No; do
	case "$answr" in 
	Yes)
	break
	;;
	No)
	exit 1
	;;
	esac
done

# Loops through all variables starting from the 6th, checks if they exist as dirs and if they don't they are created.
#Gets the last part of the repository $GitRepo.
RName="${GitRepo##*/}"
for dir in "${VarArr[@]:5}"; do
	if [[ -d "$dir" ]]; then
			
		CStat S "The directory \"$dir\" already exists. Proceeding." 
		if [[ -d "${dir}/${RName%%.*}/.git" ]]; then

			CStat F "Fatal! A git repo already exists in \"$dir\""
			exit 1
		fi
		if ! [[ -w "$dir" ]]; then

			CStat F "Fatal! You do not have permission to write to \"$dir\". Please consider running this script as root or modifying install.conf. "
			exit 1
		fi
	else
		if mkdir -p "$dir" 2>/dev/null; then
			
			CStat S "The directory \"$dir\" does not exists. Creating it."
		else
			CStat F "Fatal! Unable to create \"$dir\". Please consider running this script as root or modifying install.conf. "
			exit 1
		fi
	fi
done


if [[ "$GitRepoKey" == "TRUE" ]]; then 

	CStat C "This repository \"$GitRepo\" is set to require ssh key."
	if [[ "$Edited" == "TRUE" ]] && ! [[ "$SSHKey" == "NONE" ]]; then
			
		CStat C "Using the priv key \"$SSHKey\" specified in install.conf."
	else
		CStat I "Would you like to generate a new SSH key pair? Choosing no would allow you to specify the path of a priv key file."
		select answr2 in Yes No Exit; do 
			case $answr2 in
			Yes)
			CStat I "Please enter the full path where the private key is located"
			read -rp "(Example:/root/.ssh/repo_key):" SSHKey
			break
			;;
			No)
			CStat I "Should the key pair have password?"
			read -rsp "[Click Enter for passwordless]:" SSHPass
			SSHKey="$SSHPath/repo_key"
			ssh-keygen -t "$SSHType" -b "$SSHBits" -E sha256 -P "$SSHPass" -f "$SSHKey" > /dev/null 2>&1
			CStat S "Generating a SSH key repo_key in $SSHPath"
			break
			;;
			Exit)
			exit
			;;
			esac
		done
	fi
	
	if [[ ! -f "$SSHKey" ]]; then
	
		CStat F "Fatal! Invalid path for the ssh priv key: $SSHKey"
		exit 1
	fi
	
	PubKey="$(cat "${SSHKey}".*)"
	if [[ -n "$PubKey" ]]; then 
		
		CStat C "Please ensure that the below public key is added to your git repo:\\n\\n$PubKey\\n" 
	fi
	
	CStat I "Is your public key added to your git repo \"$GitRepo\"?"
	select answr3 in Yes No Exit; do 
		case $answr3 in
		Yes)
		break
		;;
		No)
		continue
		;;
		Exit)
		exit
		;;
		esac
	done
	
	if ssh -T "${GitRepo%%:*}" -i "$SSHKey" >/dev/null 2>&1; then

		CStat S "I was able to ssh to \"$GitRepo\"!"
		git -C "$InsPath" config --global core.sshCommand "ssh -i $SSHKey" 
	else
		CStat F "I was unable to ssh to \"$GitRepo\"! For testing run ssh -T ${GitRepo%%:*} -i $SSHKey"
		exit 1
	fi
	
elif [[ "$GitRepoKey" == "FALSE" ]]; then 

	CStat C "The $GitRepo set in install.conf does not require ssh key."
else
	CStat F "Fatal! Unknown value \"$GitRepoKey\". Please review the install.conf file."  
	exit 1
fi

CStat S "Configuring GLOBAL git user \"$GitName\", email \"$GitEmail\"and ssh key \"$SSHKey\"."
git -C "$InsPath" config --global user.name "$GitName"
git -C "$InsPath" config --global user.email "$GitEmail"
sleep 1
CStat S "Initializing git repo at $InsPath.\\n"
git -C "$InsPath" init > /dev/null 2>&1
CStat S "Clonning git repo at $InsPath.\\n"
git -C "$InsPath" clone "$GitRepo"


## Vim config - optional
if [[ "$VimConf" == "TRUE" ]]; then
	
	if ! command -v vim > /dev/null 2>&1; then
	
	CStat F "Fatal! Could not find vim. Please ensure that it is properly installed!"
	exit 1
	fi

	CStat I "Do you wish to continue with the installation of GruvBox a vim theme?"
	select answr4 in Yes No; do 
		case $answr4 in
		Yes) 
		CStat I "Please specify the home path of your user, you are running this script as $USER:"
		read -rp "[Example:/home/$USER]:" VHome
		if [[ -z "$VHome" ]]; then
			VHome="$HOME"
		fi
		CStat S "Proceeding with installation in $VHome\\n"
		mkdir -p "$VHome/.vim/autoload" "$VHome/.vim/bundle"
                curl -LSso "$VHome/.vim/autoload/pathogen.vim" https://tpo.pe/pathogen.vim
                mkdir -p "$VHome/.vim/autoload" "$VHome/.vim/bundle" 
                git clone https://github.com/morhetz/gruvbox.git "$VHome/.vim/bundle/gruvbox"
                printf  "%b" "execute pathogen#infect()\\nsyntax on\\nfiletype plugin indent on\\ncolorscheme gruvbox\\nset background=dark\\nhi! Normal ctermbg=NONE guibg=NONE\\n" > "$VHome"/.vimrc
		break
		;;
		No)
		exit
		;;
		esac
	done
else
	exit
fi





