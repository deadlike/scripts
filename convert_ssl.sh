#!/bin/bash

PFX (){
	
	local PFX
	printf "The PFX certificate has to be have been uploaded on the local machine! Current path is %s/\n" "$PWD" 	
	read -p 'Enter the absolute path to the PFX certificate:' PFX
	
	if [[ ! "$PFX" =~ "/" ]]; then

		printf "Please enter a valid path!\n"
		
	else
		printf "Converting to .pem...\n"
		openssl pkcs12 -in "${PFX}" -nodes
			
	fi

};

P7B (){
	
	echo "Enter a P7B certificate:" 
	local P7B
	P7B="$(</dev/stdin)"

	if echo -e "\n${P7B}" | openssl pkcs7 2>/dev/null 1>&2; then
	
		printf "\n\nConverting to .pem... \n"
 		echo -e "\n${P7B}" | openssl pkcs7 -print_certs; 

	else
 	
		printf "\nThis does not looks like .p7b format.\n"

	fi	
};

PEM(){

	printf "Enter a PEM certificate:\n" 
	local PEM format CHAIN PKI
	PEM="$(</dev/stdin)"
	if ! printf "%s\n\n" "$PEM" | openssl x509 -noout 2> /dev/null; then
		
		printf "This is not a pem certificate!\n"
		PEM
	fi

	printf "PEM certificates can be converted to:\n"
	select format in der pfx p7b quit; do
	
		case $format in
		
			der )
			echo -e "${PEM}\n" | openssl x509 -outform der -out CERT.der
		 	printf "\nDER certificate saved in local directory as \"CERT.der\"!\n"
			break
			;;
			pfx ) 	
			printf "You chose PFX, please enter a PKI as well, you can also choose an optional password.\n"
			PKI="$(</dev/stdin)"
			echo -e "${PKI}\n${PEM}\n" | openssl pkcs12 -export -out keystore.pfx
			printf "\nPFX certificate saved in local directory as \"keystore.pfx\"!\n"
			break
			;;		
			p7b )	
			printf "You chose p7b, you can enter an optional CA's chain certificate/s.\n"
			CHAIN="$(</dev/stdin)"
			openssl crl2pkcs7 -nocrl -out CERT.p7b -certfile <(echo -e "${PEM}\n${CHAIN}\n") 
			echo -e "\nP7B certificate saved in local directory as \"CERT.p7b\""
			break
			;;
			quit ) 
			echo "Cya"
			break
			;;
		esac
	
	done

};

main (){
	
	local FORMAT CERT
	
	printf "Enter the certificate type that you have:\n"
	select FORMAT in pem der pfx p7b Quit;do 
	
		case $FORMAT in
		
			pem) 
			PEM 
			break
			;;
			der) 
			echo "Unforunately, I have not finished the DER section :("
			break 
			;;
			pfx) 
			PFX 
			break
			;;
			p7b) 
			P7B 
			break
			;;
			Quit) 
			echo "Cya"
			exit 0
			;;
		esac
	done

	printf "\nDo you wish to convert another certificate?\n"
	select CERT in yes no;do 
	
		case $CERT in
		
			yes) 
			exec "$0"
			;;
			no)  
			printf "Cya.\n"
		  	exit 0
			;;
		esac
	done
};

main



