#!/bin/bash
GREEN='\e[1;32m'
BLUE='\033[38;5;32m'
RED='\e[1;31m'
NORMAL='\e[0;39m'   
YELLOW='\e[33m'

for z; do
	getopts "Ah" z	
		case $z in
			A)
			LIST="$HOME/scripts/RBL.long"
			;;
			h)
			printf "This scripts checks an IP address agains RBLs!\nBy default it uses the most popular RBLs, which are around 50.\nThe -A option would check against 450 RBLs.\n"
			exit 0
			;;
		esac
	(( ++OPTIND ))
done

if [[ -z $LIST ]]; then
	
	LIST="$HOME/scripts/RBL.short"
fi

for j; do
	if [[ $j =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
     
		IFS="." read -ra arr <<< "$j"
       		for i in "${arr[@]}"; do
		
		    	if [[ $i -le 255 ]]; then
			    	continue
			    else
				    printf "ERROR! Invalid IP address %s!\n" "$IP"
				    exit 1
	    		fi
		done
		IP="$j"
	else
		shift
	fi
done

if [[ -z "$IP" ]]; then

	printf "ERROR! No IP address entered!\n" 
	exit 1
fi	

rev_IP=$(printf "%s" "$IP" | awk -F'.' '{print $4"."$3"."$2"."$1}')
RBL_count="$(wc -l < "$LIST" )"
printf "Checking if %s is listed in %s RBLs:\n" "$IP" "$RBL_count"
while read -r line; do 
{
    DIG=$(dig +short "$rev_IP"."$line")
    if [[ -n "$DIG" ]]; then 
        if [[ $DIG =~ ^127\.[0-9]*\.[0-9]*\.[0-9]*$ ]]; then
        
		DIG_TXT="$(dig +short TXT "$rev_IP"."$line")"
               	printf  "%b%s.%s %b[LISTED]%b\n\tAdditional information from the RBL: %s\n" "$BLUE" "$rev_IP" "$line" "$RED" "$NORMAL" "$DIG_TXT" 

       	else 
    
         printf  "%b%s.%s %b[UNSURE:%s] %b\n" "$BLUE" "$rev_IP" "$line" "$YELLOW" "$DIG" "$NORMAL"
        
 	fi
    fi
} &
done < "$LIST"
wait








