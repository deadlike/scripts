#!/usr/bin/env python3

import os.path
from configparser import ConfigParser
from getpass import getpass


class LazyConf():

    home_dir = os.path.expanduser('~')

    config_data = ConfigParser()
    config_section = 'User Settings'
    config_user = 'user_name'
    config_pass = 'user_pass'

    def __init__(self, config_name):
        self.config_name = config_name + '.ini'
        self.config_path = os.path.join(self.home_dir, self.config_name)

    def find_config(self):
        return True if os.path.exists(self.config_path) else False

    def get_config_input(self):
        self.user_name = input('User name: ')
        self.user_pass = getpass(f'Password for {self.user_name}: ')
        return (self.user_name, self.user_pass)

    def read_config(self):
        self.config_data.read(self.config_path)
        self.user_name = self.config_data.get(self.config_section,
                                              self.config_user
                                              )
        self.user_pass = self.config_data.get(self.config_section,
                                              self.config_pass
                                              )
        return (self.user_name, self.user_pass)

    def create_config(self):
        self.config_data.add_section(self.config_section)
        self.config_data.set(self.config_section,
                             self.config_user,
                             self.user_name
                             )
        self.config_data.set(self.config_section,
                             self.config_pass,
                             self.user_pass
                             )
        with open(self.config_path, 'w') as f:
            self.config_data.write(f)


def main(conf_name='credentials_config'):
    config = LazyConf(conf_name)
    if config.find_config() is False:
        config.get_config_input()
        config.create_config()
    return config.read_config()


if __name__ == '__main__':
    main()
