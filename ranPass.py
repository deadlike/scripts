#!/usr/bin/python3

import random
import re
import string
import sys

for arg in sys.argv:
    if arg.lower() == '-p':
        sys.argv.remove(arg)
        Str = string.ascii_letters + string.punctuation + string.digits
        strength = re.compile(r'^[A-Za-z0-9!"#$%&\'()*+,-./:;<=>?@[\]^_`{|}~]{8,}$')
        break
    else:
        Str = string.ascii_letters + string.digits
        strength = re.compile(r'^[A-Za-z0-9]{8,}$')
try:
   num = int(sys.argv[1])
   length = int(sys.argv[2])
   if length < 8:
       print('Invalid length: {}. Adjusted to minimum of 8'.format(length))
       length = 8
except IndexError:
    num = 10
    length = 16

password = ''
passwords = []
insec = 0
while len(passwords) < num:
    password = ''
    while len(password) < length:
        password += random.SystemRandom().choice(Str)
    sec = strength.search(password)
    if sec == None:
        insec += 1
        continue
    else:
        passwords.append(password)

print('\nGenerating'.rjust(48,'='),num,length,'character passwords:\n')
for Pass in range(len(passwords)):
    print(str(Pass + 1).rjust(2)+')',passwords[Pass])
print('\nNumber of insecure passwords generated and then replaced:'.rjust(118,'='), insec)
