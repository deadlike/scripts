#!/bin/bash

#Checks if a domain's O365 record resolve.
NORMAL='\033[0;39m'
GREEN='\e[0;33m'
WHITE='\e[1;34m'
YELLOW='\e[1;37m'
RED='\e[1;31m'


F(){
	if [[ ! $? -eq 0 ]]; then 
		
		#echo -e "${RED}This record is missing!"; 
		printf 	"%bThis record is missing!\n" "$RED"
	
	fi
};

clear
if [[ "$1" =~ "." ]] ;then

	DOMAIN="$1"
	shift

elif [[ "$#" -eq 0 ]]; then
	
	printf "Who runs a scrit without arguments?\nUse -h for help.\n"
	exit 1;
else 

	printf "You did not enter a valid domain!\nUse -h for help.\n"

fi

for i; do
	while getopts "A:" opt; do
	case $opt in
		A) 
		shift	
		NS="@${1}"
		printf "%bThis is an authoritative query for %s in %s!\n" "$RED" "$DOMAIN" "$1" 
		break	
		;;
		*) 
		printf "\n\tThis script resolves the most common O365 records for a domain.\n\tOption:\n\t-A name server [get an authoritative answer]\n\n\tUsage:\n\t %s domain.tld\n\t %s domain.tld -A ns1.domain.tld\n\n" "${0##*/}" "${0##*/}"	
		exit 1
		;;
	esac
	done
done

if [[ -z "$NS" ]];then
	NS=" "
fi

printf "%b1.MX should point to:%b domain-tld.mail.protection.outlook.com %b\n" "$YELLOW" "$WHITE" "$GREEN"
dig  +noall +answer MX "${DOMAIN}" "$NS" | grep -Ei "^${DOMAIN}"
F

printf "%b2.OWA should point to:%b email.secureserver.net %b\n" "$YELLOW" "$WHITE" "$GREEN"
dig  +noall +answer email."${DOMAIN}" "$NS" | grep -Ei "^email.${DOMAIN}"
F

printf "%b3.Autodiscover should point to:%b autodiscover.outlook.com %b\n" "$YELLOW" "$WHITE" "$GREEN"
dig +noall +answer autodiscover."${DOMAIN}" "$NS"   | grep -Ei "^autodiscover.${DOMAIN}"
F

printf "%b4.Sip should point to:%b sipdir.online.lync.com %b\n" "$YELLOW" "$WHITE" "$GREEN"
dig +noall +answer sip."${DOMAIN}" "$NS" |  grep -Ei "^sip.${DOMAIN}"
F

printf "%b5.Lyncdiscover should point to:%b webdir.online.lync.com %b\n" "$YELLOW" "$WHITE" "$GREEN"
dig +noall +answer lyncdiscover."${DOMAIN}" "$NS" | grep -Ei "^lyncdiscover.${DOMAIN}"
F

printf "%b6.Msoid should point to:%b clientconfig.microsoftonline-p.net %b\n" "$YELLOW" "$WHITE" "$GREEN"
dig +noall +answer msoid."${DOMAIN}" "$NS" | grep -Ei "^msoid.${DOMAIN}"
F

printf "%b7.Enterpriseregistration should point to:%b enterpriseregistration.windows.net %b\n" "$YELLOW" "$WHITE" "$GREEN"
dig +noall +answer enterpriseregistration."${DOMAIN}" "$NS" | grep -Ei "^enterpriseregistration.${DOMAIN}"
F

printf "%b8.Enterpriseenrollment should point to:%b enterpriseenrollment.manage.microsoft.com %b\n" "$YELLOW" "$WHITE" "$GREEN"
dig +noall +answer enterpriseenrollment."${DOMAIN}" "$NS" | grep -Ei "^enterpriseenrollment.${DOMAIN}"
F

printf "%b9.TXT records for the domain:%b SPF: \"v=spf1 include:spf.protection.outlook.com -all\"%b \n" "$YELLOW" "$WHITE" "$GREEN"
dig +noall +answer TXT "${DOMAIN}" "$NS" | grep -Ei "^${DOMAIN}"
F

printf "%b10.Dmarc should point to something like:%b v=DMARC1;p=none;sp=none;rua=mailto:postmaster@domain.tld;ruf=mailto:postmaster@domain.tld;fo=1 %b\n" "$YELLOW" "$WHITE" "$GREEN"
dig +noall +answer TXT _dmarc."${DOMAIN}" "$NS" | grep -Ei "^_dmarc.${DOMAIN}"
F

printf "%b11.DKIM should point to something like:%b v=DKIM1\; g=*\; k=rsa\; p=(long base 64 encoded string) %b\n" "$YELLOW" "$WHITE" "$GREEN"
dig TXT +noall +answer default._domainkey."${DOMAIN}" "$NS" | grep -Ei "^default._domainkey.${DOMAIN}" 
F

printf "%b12._sip should point to:%b sipdir.online.lync.com %b\n" "$YELLOW" "$WHITE" "$GREEN"
dig SRV +noall +answer _sip._tls."${DOMAIN}" "$NS" | grep -Ei "^_sip._tls.${DOMAIN}"
F

printf "%b13._sipfederationtls should point to:%b sipfed.online.lync.com %b\n" "$YELLOW" "$WHITE" "$GREEN"
dig SRV +noall +answer _sipfederationtls._tcp."${DOMAIN}" "$NS" | grep -Ei "^_sipfederationtls._tcp.${DOMAIN}"
F


printf	"\n\n%bDisclaimer:\n%bNOT all of the above queried records are necassary for O365 to function!\n%b" "$YELLOW" "$RED" "$NORMAL"

