#!/bin/bash

#Reads CERTS and CSRs
#Made by Vic U.
#Color codes
BLACK="\033[30m"
RED="\033[31m"
GREEN="\033[32m"
YELLOW="\033[33m"
BLUE="\033[34m"
PINK="\033[35m"
CYAN="\033[36m"
WHITE="\033[37m"
NORMAL="\033[0;39m"
WHITE='\e[1;37m'
LIGHT_GREEN='\e[1;32m'
LIGHT_BLUE='\e[1;34m'
LIGHT_RED='\e[1;31m'
LIGHT_PURPLE='\e[1;35m'

SSL_info(){

	local TEXT
	TEXT="$( printf "%b" "$1" | sed '/^$/d' )"

if printf "%s\n\n" "$TEXT"  | openssl x509 -noout ; then
     
     	printf "%s\n\n" "$TEXT" | openssl x509 -noout -text -dates -issuer -subject

elif printf "%s\n\n" "$TEXT" | openssl req -noout ; then
     
	printf "%s\n\n" "$TEXT" | openssl req -text -noout                              

elif printf "%s\n\n" "$TEXT" | openssl pkcs7 -noout; then 
	
	printf "%s\n\n" "$TEXT" | openssl pkcs7 -print_certs -print -text

else
     
     	printf "\n%bLooks like that ain't a CERT/CSR. Try again :) !%b\n\n" "$LIGHT_RED" "$NORMAL" >&2
     	exec "$0";

fi 2> /dev/null

}


printf "%bSupported formats: .pem;.p7b;.csr;\n\nEnter a SSL %bCERT/CSR%b, once you are ready%b make sure the cursor is on a new line%b, then click %bCTRL +D:\n%b" "$WHITE" "$RED" "$WHITE" "$RED" "$WHITE" "$RED" "$NORMAL"


if [[ "$#" -gt 0 ]]; then

	for i; do
		if [[ ! -s "$i" ]]; then
		     break;
     		fi	     
	SSL_info "$(cat "$i" 2> /dev/null)"		
	done | more
else

	TEXT="$(</dev/stdin)"
	SSL_info "$TEXT"
fi


